# Overview

[![Wemos D1 mini soil moisture sensor & relay module](https://img.youtube.com/vi/FyDMSy-I9f0/maxresdefault.jpg)](http://www.youtube.com/watch?v=FyDMSy-I9f0 "Wemos D1 mini soil moisture sensor & relay module")

  This project contains the minimun files needed to start a SmartThings app for the Soil Moisture Sensor and Relay Prototype. Contains two files: a rule-chain and a dashboard.

  For furder information on rule chains you can read the [Rule Chain User Guide](https://thingsboard.io/docs/user-guide/ui/rule-chains/) from ThingsBoard docs. Docs on dashboards can be found on [Dashboard User Guide](https://thingsboard.io/docs/user-guide/ui/rule-chains/)




## Watering Rule Chain.  

This rule chain receives telemetry data and evaluates the humidity value. Once the humidity value drops from a given threshold it raises an alarm and sends a RPC to the device to start watering. The RPC name is setValveStatus and the RPC param is `true`.

The device then receive the RPC and process it. It switch the valve on/off depending on the RPC params.

If the humidity grows from threshold, it clear previous alarm and sends a RPC to the device to stop watering. The RPC name is the same but the RPC param is `true`. 

![Wemos D1 watering rule chain](rule-chains/wemos_d1_watering_rule_chain.png)

## Dashboard

The dashboard is made of some individual charts and buttons as shown below:

![Wemos D1 watering dashboard](dashboards/wemos_d1_watering_dashboard-on.png)

The current used widgets are the following:
  * Alarm monitor. This panel displays info about alarms related to the device.
  * LED indicator. Shows if the watering pump is switched on or off.
  * Switch. Allows manually switch the watering pump on of off.
  * Humidity telemetry timeseries. Displays the last humidity values.


    